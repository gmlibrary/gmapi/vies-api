import http.client
import json
import re
from time import sleep

logbooktxtName = "logbook.txt"
EuCOuntriesDict = { 
   "austria":"AT",
   "belgium":"BE", 
   "bulgaria":"BG", 
   "croatia":"HR",
   "republic of cyprus" :"CY", "cyprus":"CY",
   "czech republic":"CZ", "czechia":"CZ",
   "denmark":"DK", 
   "estonia":"EE", 
   "finland":"FI", 
   "france":"FR", 
   "germany":"DE", 
   "greece":"EL", 
   "hungary":"HU", 
   "ireland":"IE", 
   "italy":"IT", 
   "latvia":"LV", 
   "lithuania":"LT", 
   "luxembourg":"LU", 
   "malta":"MT", 
   "netherlands":"NL",
   "poland":"PL", 
   "portugal":"PT", 
   "romania":"RO", 
   "slovakia":"SK", 
   "slovenia":"SL", 
   "spain":"ES",
   "sweden":"SE",
   "northen ireland":"XI"
}


def iscountryEU(country):
    mycompanyCountry = country.strip().lower()
    if mycompanyCountry in EuCOuntriesDict:
        return True
    else:
        return False
    
def euCountryCode(country):
    mycountry=country.strip().lower()
    if iscountryEU(mycountry):
        return EuCOuntriesDict[mycountry]
    else:
        return False

def validateVat (aVat, controlTextOn):
#  logEntry( "\n", logbooktxtName) 
  print("Estou em VIES_JM this is the avat : "+ str(aVat) + " --> validateVat()")
  if not aVat:
    print( "------> END : " + "Vat was null = "+ str(aVat) +". Did not call VIES.")
    return "No call - Vat null"
  else: 
    #variables
    items = ""
    rxpfinal=""
    CountryCode="TT"
    VATnumber="12345678"
    thisVat = aVat.replace(" ", "")
    
    CountryCode = thisVat[:2]
    VATnumber = thisVat[2:]
    
    #CALL
    print( "-> Call : " + " CountryCode " + CountryCode + " VATnumber " + VATnumber)
    conn = http.client.HTTPSConnection("ec.europa.eu", timeout=30)
    payload = ''
    headers = {}

    conn.request("GET", "/taxation_customs/vies/rest-api/ms/"+CountryCode+"/vat/"+VATnumber, payload, headers)
    res = conn.getresponse()
    data = res.read()
  
    rxp = data.decode("utf-8")
    rxpList =json.loads(rxp)
    print( "-> Call rxp : " + str(rxpList))     
    
    try:
      rxpfinal = rxpList.get('userError')
      print( "END : " + " userError : " + rxpfinal)   
    except:  
      pass
      
    try:
      rxpfinal = rxpList.get('errorWrappers')[0].get('message')
      print( "END : " + " errorWrappers : " + rxpfinal ) 
    except:
      pass
    
  return rxpfinal
